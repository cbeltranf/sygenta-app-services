<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Newproducts_type extends Model
{
    protected $table = "fn_products_type";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    protected $fillable = ['id','name','image','status'];
}
