<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Phases extends Model
{
    use SoftDeletes;
    protected $table = "crops_phases";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    protected $dates = ['deleted_at'];
    protected $fillable = ['id','crops_id','name','status','period','img'];
    protected $appends = ['type_products'];

    public function getTypeProductsAttribute()
    {
        $return = array();
        $item = DB::table('products_type')
                ->join('products', 'products.product_type_id', '=', 'products_type.id')
                ->join("products_x_crops_phase", 'products_x_crops_phase.products_id', '=', 'products.id')
                ->where("products_x_crops_phase.crops_phases_id", "=",  $this->id)
                ->where("products.status", "=",  "A")
                ->where("products_type.status", "=", 'A')
                ->select('products_type.*')
                ->groupBy('products_type.id')
                ->get()->toArray();
                $count = 0;
        foreach($item as $k => $v){
            $return[$count] = $v;
            $return[$count]->products = DB::table('products')
            ->join('products_type', 'products.product_type_id', '=', 'products_type.id')
            ->join("products_x_crops_phase", 'products_x_crops_phase.products_id', '=', 'products.id')
            ->where("products_x_crops_phase.crops_phases_id", "=",  $this->id)
            ->where("products_type.id", "=",  $v->id)
            ->where("products.status", "=",  "A")
            ->where("products_type.status", "=", 'A')
            ->select('products.*')
            ->get();
            $count++;
        }
        return $return;
    }


    public function Crop()
    {
        return $this->belongsTo('App\Models\Crops', 'crops_id', 'id');
    }



}
