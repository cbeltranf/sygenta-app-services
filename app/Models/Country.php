<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Country extends Model
{
    use SoftDeletes;
    protected $table = "it_countries";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $dates = ['deleted_at'];
    protected $fillable = ['name','code','iso','nicename','iso3','numcode','phonecode'];


}