<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Crops extends Model
{
    protected $table = "crops";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    protected $fillable = ['id','name','status', 'order', "it_countries"];


    public function Phases()
    {
        return $this->hasMany('App\Models\Phases', 'crops_id', 'id')->orderBy('order');
    }

}
