<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Products extends Model
{
    protected $table = "products";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    protected $dates = ['deleted_at'];
    protected $fillable = ['name','product_type_id','status', 'img', 'target','dose','positioning','composition','formulation','ica','tips'];
    protected $appends = ['type'];


    public function getTypeAttribute()
    {
        $item = DB::table('products_type')->where("id", "=",  $this->product_type_id)->first();
        return  $item;
    }


}
