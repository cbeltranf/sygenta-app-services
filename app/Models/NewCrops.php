<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class NewCrops extends Model
{
    protected $table = "fn_crops";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    protected $fillable = ['id','name','image','status'];

}
