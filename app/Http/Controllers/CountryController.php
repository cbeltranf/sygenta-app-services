<?php

namespace App\Http\Controllers;

use App\Models\Country;
use Illuminate\Http\Request;
use App\Http\Resources\GlobalCollection;
use DB;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      
        $flags = DB::table('it_countries')
                     ->select(DB::raw("DISTINCT(it_countries.id), it_countries.name, CONCAT('".env("APP_URL")."', it_countries.flag) as flag") )
                     ->join('fn_crops_countries', 'it_countries.id', '=', 'fn_crops_countries.it_countries_id')
                     ->where('it_countries.status', '=', "A")
                     ->where('fn_crops_countries.status', '=', "A")
                     ->orderBy('it_countries.name', 'asc')
                     ->get();

        return new GlobalCollection($flags);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Crops  $crops
     * @return \Illuminate\Http\Response
     */
    public function show($crops)
    {
        $crops =  Crops::with('Phases')->find($crops);
        return response()->json($crops);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Crops  $crops
     * @return \Illuminate\Http\Response
     */
    public function edit(Crops $crops)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Crops  $crops
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Crops $crops)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Crops  $crops
     * @return \Illuminate\Http\Response
     */
    public function destroy(Crops $crops)
    {
        //
    }
}
