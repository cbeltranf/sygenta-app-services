<?php

namespace App\Http\Controllers;

use App\Models\Crops;
use Illuminate\Http\Request;
use App\Http\Resources\GlobalCollection;
use DB;

class CropsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filterValue = $request->input("filterValue");
        $pageSize = $request->input("pageSize");
        $sortOrder = ($request->input("sortOrder") == "asc") ? "asc" : "desc";
        $filter =  $request->input("filter");
        $sortField = "name";

        $Auth_user = auth('api')->user();

        $item = Crops::orderBy($sortField, $sortOrder);
        
        if (isset($Auth_user->id) && !empty($Auth_user->id)){            
            $user = DB::table('it_users')->where("id","=",$Auth_user->id)->get();
            $city = $user[0]->it_countries_id;
            $item->where('it_countries', '=', $city);
        }
        $item->with('Phases');

        if (!empty($filterValue)) {
            $item->where($filter, 'like', "%$filterValue%");
        }

        if (empty($pageSize)) {
            $pageSize = 20;
        }

        return new GlobalCollection($item->paginate($pageSize));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Crops  $crops
     * @return \Illuminate\Http\Response
     */
    public function show($crops)
    {
        $crops =  Crops::with('Phases')->find($crops);
        return response()->json($crops);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Crops  $crops
     * @return \Illuminate\Http\Response
     */
    public function edit(Crops $crops)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Crops  $crops
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Crops $crops)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Crops  $crops
     * @return \Illuminate\Http\Response
     */
    public function destroy(Crops $crops)
    {
        //
    }
}
