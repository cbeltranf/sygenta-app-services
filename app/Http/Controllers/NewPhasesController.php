<?php

namespace App\Http\Controllers;

use App\Models\NewCrops;
use Illuminate\Http\Request;
use App\Http\Resources\GlobalCollection;
use DB;

class NewPhasesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $filterValue = $request->input("category_id");


        $items = DB::table('fn_crops_phases')
                     ->select(DB::raw("fn_crops_phases.id, fn_phases.name AS phase_name,  CONCAT('".env("APP_URL")."', fn_crops_phases.img) AS phase_image, fn_periods.name AS period_name, fn_crops_phases.* ") )
                     ->join('fn_phases', 'fn_phases.id', '=', 'fn_crops_phases.fn_phases_id')
                     ->join('fn_periods', 'fn_periods.id', '=', 'fn_crops_phases.fn_periods_id')
                     ->where('fn_phases.status', '=', "A")
                     ->where('fn_periods.status', '=', "A")
                     ->where('fn_crops_phases.status', '=', "A")
                     ->where('fn_crops_phases.fn_crops_countries_category_id', '=', $filterValue)
                     ->orderBy('fn_crops_phases.order', 'asc')
                     ->get();
        foreach($items as $it_k => $item){

            $items_cat = DB::table('fn_products_type')
            ->select(DB::raw("fn_products_type.name AS type_name, fn_products_type.id AS type_id,  CONCAT('".env("APP_URL")."', fn_products_type.image) AS type_image, CONCAT('".env("APP_URL")."', fn_products.img) AS product_image,fn_products_phases.target, fn_products_phases.dose, fn_products_phases.positioning, fn_products_phases.composition, fn_products_phases.formulation, fn_products_phases.ica, fn_products_phases.tips, fn_products.*") )
            ->join('fn_products', 'fn_products.fn_products_type_id', '=', 'fn_products_type.id')
            ->join('fn_products_phases', 'fn_products.id', '=', 'fn_products_phases.fn_products_id')

            ->where('fn_products_type.status', '=', "A")
            ->where('fn_products_phases.status', '=', "A")
            ->where('fn_products.status', '=', "A")
            ->where('fn_products_phases.fn_crops_phases_id', '=', $item->id)
            ->orderBy('fn_products_type.name', 'asc')
            ->orderBy('fn_products.name', 'asc')
            ->get();

            $types = [];

            foreach($items_cat as $kc => $vc){
                $types[$vc->type_id]["type"] = $vc->type_name;
                $types[$vc->type_id]["type_image"] = $vc->type_image;
                $types[$vc->type_id]["products"][$vc->id]["name"] = $vc->name;
                $types[$vc->type_id]["products"][$vc->id]["image"] = $vc->product_image;
                $types[$vc->type_id]["products"][$vc->id]["target"] = $vc->target;
                $types[$vc->type_id]["products"][$vc->id]["dose"] = $vc->dose;
                $types[$vc->type_id]["products"][$vc->id]["positioning"] = $vc->positioning;
                $types[$vc->type_id]["products"][$vc->id]["composition"] = $vc->composition;
                $types[$vc->type_id]["products"][$vc->id]["formulation"] = $vc->formulation;
                $types[$vc->type_id]["products"][$vc->id]["ica"] = $vc->ica;
                $types[$vc->type_id]["products"][$vc->id]["tips"] = $vc->tips;

            }

            $items[$it_k]->types = $types;

        }

        $crop_info = DB::table('fn_crops_countries_category')
        ->selectRaw("CONCAT('".env("APP_URL")."',fn_crops.image) as image, CONCAT('".env("APP_URL")."',fn_crops_countries_category.crops_phase_pdf) as crops_phase_pdf,  CONCAT('".env("APP_URL")."',fn_crops_countries.pdf_lmr) as pdf_lmr")
        ->join('fn_crops_countries', 'fn_crops_countries.id', '=', 'fn_crops_countries_category.fn_crops_countries_id')
        ->join('fn_crops', 'fn_crops.id', '=', 'fn_crops_countries.fn_crops_id')
        ->where('fn_crops_countries_category.id', '=', $filterValue)
        ->first();

        $out = [
            'data' => $items,
            'crop' => $crop_info,
            'links' => [
                'self' => 'link-value',
            ],
        ];

        return response()->json($out);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Crops  $crops
     * @return \Illuminate\Http\Response
     */
    public function show($crops)
    {
        $crops =  Crops::with('Phases')->find($crops);
        return response()->json($crops);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Crops  $crops
     * @return \Illuminate\Http\Response
     */
    public function edit(Crops $crops)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Crops  $crops
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Crops $crops)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Crops  $crops
     * @return \Illuminate\Http\Response
     */
    public function destroy(Crops $crops)
    {
        //
    }
}
