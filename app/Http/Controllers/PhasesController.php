<?php

namespace App\Http\Controllers;
use App\Http\Resources\GlobalCollection;
use App\Models\Phases;
use Illuminate\Http\Request;

class PhasesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->input("filter");
        $filterValue = $request->input("filterValue");
        $pageSize = $request->input("pageSize");
        $sortOrder = ($request->input("sortOrder") == "asc") ? "asc" : "desc";
        $sortField = "id";

        $item = Phases::orderBy($sortField, $sortOrder);
        $item->with('Crop');

        if (!empty($filterValue)) {
            $item->where($filter, 'like', "%$filterValue%");
        }

        if (empty($pageSize)) {
            $pageSize = 20;
        }

        return new GlobalCollection($item->paginate($pageSize));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Phases  $phases
     * @return \Illuminate\Http\Response
     */
    public function show($phases)
    {
        $phases =  Phases::with('Crop')->find($phases);
        return response()->json($phases);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Phases  $phases
     * @return \Illuminate\Http\Response
     */
    public function edit(Phases $phases)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Phases  $phases
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Phases $phases)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Phases  $phases
     * @return \Illuminate\Http\Response
     */
    public function destroy(Phases $phases)
    {
        //
    }
}
