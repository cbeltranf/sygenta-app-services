<?php

namespace App\Http\Controllers;

use App\Models\NewCrops;
use Illuminate\Http\Request;
use App\Http\Resources\GlobalCollection;
use DB;

class NewCropsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filterValue = $request->input("countries_id");


        $items = DB::table('fn_crops')
                     ->select(DB::raw("DISTINCT(fn_crops_countries.id), fn_crops.name, CONCAT('".env("APP_URL")."', fn_crops.image) as image, fn_crops_countries.pdf_lmr") )
                     ->join('fn_crops_countries', 'fn_crops.id', '=', 'fn_crops_countries.fn_crops_id')
                     ->where('fn_crops.status', '=', "A")
                     ->where('fn_crops_countries.status', '=', "A")
                     ->where('fn_crops_countries.it_countries_id', '=', $filterValue)
                     ->orderBy('fn_crops.name', 'asc')
                     ->get();
        foreach($items as $it_k => $item){

            $items_cat = DB::table('fn_crops_countries_category')
            ->select(DB::raw("fn_crops_countries_category.id, fn_categories.name") )
            ->join('fn_categories', 'fn_categories.id', '=', 'fn_crops_countries_category.fn_categories_id')
            ->where('fn_categories.status', '=', "A")
            ->where('fn_crops_countries_category.status', '=', "A")
            ->where('fn_crops_countries_category.fn_crops_countries_id', '=', $item->id)
            ->orderBy('fn_categories.name', 'asc')
            ->get();

            $items[$it_k]->categories = $items_cat;

        }

        return new GlobalCollection($items);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Crops  $crops
     * @return \Illuminate\Http\Response
     */
    public function show($crops)
    {
        $crops =  Crops::with('Phases')->find($crops);
        return response()->json($crops);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Crops  $crops
     * @return \Illuminate\Http\Response
     */
    public function edit(Crops $crops)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Crops  $crops
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Crops $crops)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Crops  $crops
     * @return \Illuminate\Http\Response
     */
    public function destroy(Crops $crops)
    {
        //
    }
}
