<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['api', 'jwt'])->group(function () {
    Route::group([
        'prefix' => 'v1'
    ], function () {
        Route::apiResource('phases', 'PhasesController');
        Route::apiResource('products', 'ProductsController');
        Route::apiResource('crops', 'CropsController');
        Route::apiResource('crops_cities', 'CountryController');
        Route::apiResource('new_crops', 'NewCropsController');
        Route::apiResource('new_phases', 'NewPhasesController');
        Route::apiResource('product_type', 'Product_typesController');
    });
});
