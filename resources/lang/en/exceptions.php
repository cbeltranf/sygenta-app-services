<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'token_invalid'   => 'Token Invalid',
    'token_expired' => 'Token Expired',
    'token_problem' => 'Token Problem. JWT',
    'unauthenticated' => 'Unauthenticated',
    'route_not_valid' => 'Route Not Valid',
    'element_not_found' => 'Element Not Found',
    'query_exception' => 'Restriction detected in database',
    'nocellphoneregistered' => 'We do not have your cell phone number registered in the system. Please choose another method of recovery'
];
