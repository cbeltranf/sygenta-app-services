<?php

return [
    'ok'   => 'ok.',
    'justnow'   => 'Right now',
    'aminute' => "A minute ago",
    'timeago' => "",
    'timeminutes' => "minutes ago",
    'ahour' => "An hour ago",
    'timehours' => "hours ago",
    'yesterday' => "Yesterday",
    'timedays' => "days ago",
    'week' => "A week ago",
    'timeweek' => "weeks ago",
    'month' => "A month ago",
    'timemonth' => "months ago",
    'year' => "A year ago",
    'timeyear' => "years ago",



];
