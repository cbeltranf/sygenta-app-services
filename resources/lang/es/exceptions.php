<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'token_invalid'   => 'Tu sesión ha expirado, por favor inicia sesión nuevamente.',
    'token_expired' => 'El Token ha expirado',
    'token_problem' => 'Ha ocurrido un problema con el Token. JWT',
    'unauthenticated' => 'No autenticado.',
    'route_not_valid' => 'Ruta no valida.',
    'element_not_found' => 'Elemento inexistente',
    'query_exception' => 'Restricción encontrada en BD.',
    'nocellphoneregistered' => 'No tenemos tu numero celular registrado en el sistema. Por favor escoge otro metodo de recuperación'
];
